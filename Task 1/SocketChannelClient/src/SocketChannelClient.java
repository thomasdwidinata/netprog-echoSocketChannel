import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Scanner;

public class SocketChannelClient {
	
	public static void main(String[] args) {
		
		// Declare variables to save the port and host (And it's defaults too!)
		int port = 8080;
		String host = "localhost";
	
		// Try to parse and detect the arguments given by the User
		try {
			switch(args.length) {
			case 2: // If given 2 arguments, making sure that the second argument is the port and a valid port number range
				port = Integer.parseInt(args[1]);
				if(port < 1 || port > 65535)
					throw new NumberFormatException("Invalid Port Number");
				host = args[0];
				break;
			default: // If no argument or invalid numbers of arguments given, just use the default one
				System.err.println("Invalid arguments. First argument should be the host and the second one is the host port! Using the default one...");
				break;
			}
		} catch (NumberFormatException e) { // If error raised, reset to the default value
			System.err.println("Invalid arguments. First argument should be the host and the second one is the host port! Using the default one...");
			port = 8080;
			host = "localhost";
		}
		
		// Declare SocketChannel Connection variable and important variables
		SocketChannel channel = null; // The socket channel, for connecting to the server
		ByteBuffer buffer = ByteBuffer.allocate(2048); // Declare Buffer with size of 2KB
		String data = new String(); // For saving the received data from Server
		CharBuffer chars = null; // For converting from ByteBuffer to String
		Scanner scan = new Scanner(System.in); // Declare Keyboard Input
		
		try {
			channel = SocketChannel.open(); // Open new Channel
			channel.configureBlocking(false); // Set non blocking mode
			channel.connect(new InetSocketAddress(host, port)); // Connect to desired host
			
			while(! channel.finishConnect() ){
			    System.out.println("Connecting to " + host + ":" + port + "..."); 
			}
			
			while(true) {
				// Housekeeping here, Cleaning up cache and buffer!
				buffer.clear(); // Clear buffer each time entering the loop
				data = new String(); // Clean the data variable
				
				System.out.printf("Enter a text to be sent to the server: ");
				data = scan.nextLine(); // Scan input from user's keyboard
				buffer.put(data.getBytes()); // Place `data` into the buffer by giving array of bytes
				buffer.flip(); // Give a stop pointer to the buffer, indicating that the buffer are only that much
				channel.write(buffer); // Sends the data to the server (From buffer to server)
				buffer.clear(); // Clear the buffer after using
				
				// If buffer still empty, wait until it fills
				while(channel.read(buffer) < 1) {
					buffer.clear();
				}
				
				buffer.flip(); 
				chars = Charset.forName("ISO-8859-1").decode(buffer); // Decode the Buffer to ASCII
				System.out.println("Got response from server : " + chars.toString());
				if(chars.toString().toLowerCase().equals("x")) // If string is 'x' (lowered case), exit!
				{
					System.out.println("Exiting...");
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		scan.close(); // Close the scanner (Keyboard input)
	}
}
