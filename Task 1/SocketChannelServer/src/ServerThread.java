import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

public class ServerThread implements Runnable{

	// Declare important variables
	private SocketChannel client = null; // For saving the client
	private static final int BUFFER_SIZE = 2048; // For setting up the buffer size to 2KB
	private ByteBuffer buffer = ByteBuffer.allocate(ServerThread.BUFFER_SIZE); // Allocate the buffer to read what client says
	private CharBuffer chars = null; // For converting from ByteBuffer to String
	
	public ServerThread(SocketChannel client) {
		this.client = client;
	}
	
	@Override
	public void run() {
		try {
			while(true){
				buffer.clear(); // Cleanup!
				if(this.client.read(buffer) < 0) { // If -1, the connection was dropped...
					System.out.println("Connection has been closed...");
					break; // exit the application
				}
				buffer.flip(); // After reading it, set a flag that we only need that much of data
				chars = Charset.forName("ISO-8859-1").decode(buffer); // Decode the bytes to CharBuffer
				System.out.printf("[  i  ] [ %s ] Got String %s\n", this.client.getRemoteAddress(), chars.toString().toLowerCase());
				buffer.clear();
				buffer.put(chars.toString().toLowerCase().getBytes()); // Fill the buffer with the response, and lower case... 
				buffer.flip();
				this.client.write(buffer); // Sends the buffer to client
				buffer.clear(); // Cleanup after use!
				if(chars.toString().isEmpty()) {
					System.out.println("Empty String detected...");
				}
			}
			System.out.printf("[  !  ] [ %s ] Client disconnected\n", this.client.getRemoteAddress());
			this.client.close(); // Close connection is finish using it. And Thread will stop itself.
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
