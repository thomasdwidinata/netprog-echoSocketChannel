import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

public class SocketChannelServerNonBlocking {

	public static final int BUFFER_SIZE = 2048; // For setting up the buffer size to 2KB
	
	public static void main(String[] args) {
		
		// Declare variables to save the port (And it's defaults too!)
		int port = 8080;
		
		// Try to parse and detect the arguments given by the User
		if(args.length == 1) { // If no argument given, just use the default one
			try {
				port = Integer.parseInt(args[0]);
				if(port < 1 || port > 65535) // If the port number is invalid, raise exception to use the default one
					throw new NumberFormatException("Invalid Port Number");
			} catch (NumberFormatException e) {
				port = 8080;
				System.err.println("Invalid port has been given. Will be using default port.");
			}
		}
		
		// Declare Important Variables
		ServerSocketChannel serverSocketChannel = null; // SocketChannel that serves the clients. Because it is a ServerSocketChannel!
		SocketChannel socketChannel = null; // For saving the Connected Clients 
		Selector select = null; // Selector to select the list of clients, and identify which one is ready
		CharBuffer chars = null; // For converting from ByteBuffer to String
		String response = new String(); // For saving client's response 
		boolean loop = true; // To detect whether the server should terminate or not

		try {
			// Open the sockets and begin serving clients
			serverSocketChannel = ServerSocketChannel.open();
			serverSocketChannel.configureBlocking(false); // Set to non blocking mode, which is why we need Selector to select clients
			serverSocketChannel.socket().bind(new InetSocketAddress(port)); // Reserve the port
			select = Selector.open(); // Open the selector
			
			// Register defined selector to the ServerSocketChannel, and second parameter is to determine which one
			// that should be registered into the selection. Then let this thing handle the rest to accept clients
			serverSocketChannel.register(select, SelectionKey.OP_ACCEPT);
			System.out.println("[  i  ] Server has started with address " + serverSocketChannel.getLocalAddress());
			
			while(loop) { // Server Loop
				 if(select.select() == 0) { // If no one has connected, loop until someone comes...
					 Thread.sleep(50);
					 continue;
				 }
				 Set<SelectionKey> readyKeys = select.selectedKeys(); //Get the sets of selection keys, it's a collection so we must iterate it
				 Iterator<SelectionKey> iterator = readyKeys.iterator(); // Get the iterator for easier looping and iterating 
				 while( iterator.hasNext() ) { // While there are still more keys
					SelectionKey key = iterator.next(); // Get the next Key (A key is the object of the socketchannel, basically a client)
					iterator.remove(); // Remove to prevent duplicate, it will be added again slater on
					if(key.isAcceptable()) { // Check whether the client is valid and still connected
						ServerSocketChannel server = (ServerSocketChannel) key.channel(); // Get the Server instance
						SocketChannel client = server.accept(); // Get the Client
						client.configureBlocking(false); // Set as non blocking
						client.register(select, SelectionKey.OP_READ); // After accepting it, read it! Set the read flag, at this stage, the client registered back to the selector
						continue;
					}
					if(key.isReadable()) { // If the selected key is readable (When it is set by SelectionKey.OP_READ) then run thread
						SocketChannel client = (SocketChannel) key.channel(); // Get the Client
						ByteBuffer buffer = ByteBuffer.allocate(SocketChannelServerNonBlocking.BUFFER_SIZE); // Allocate the buffer to read what client says
						if(client.read(buffer) < 0) { // If -1, the connection was dropped...
							System.out.println("[  !  ] Connection has been closed...");
							client.register(select, SelectionKey.OP_CONNECT); // To ignore to client, because it is not used anymore
							client.close();
							break;
						}
						buffer.flip(); // After reading it, set a flag that we only need that much of data
						chars = Charset.forName("ISO-8859-1").decode(buffer); // Decode the bytes to CharBuffer
						System.out.printf("[  i  ] [ %s ] Got String %s\n", client.getRemoteAddress(), chars.toString());
						response = chars.toString(); // Save the response, for writing it later
						buffer.clear(); // Cleanup!
						client.register(select, SelectionKey.OP_WRITE); // After reading, it's ready to write!
					}
					if (key.isWritable()) {
						  SocketChannel client = (SocketChannel) key.channel();
						  ByteBuffer buffer = ByteBuffer.allocate(SocketChannelServerNonBlocking.BUFFER_SIZE);
						  buffer.clear();
						  buffer.put(response.getBytes()); // Fill the buffer with the response to be echoed back
						  buffer.flip();
						  client.write(buffer); // Send to client
						  buffer.clear(); // cleanup!
						  client.register(select, SelectionKey.OP_READ); // After writing, read it!
					}
				 }
			}
			serverSocketChannel.close(); // Safely close the server
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
	
}
