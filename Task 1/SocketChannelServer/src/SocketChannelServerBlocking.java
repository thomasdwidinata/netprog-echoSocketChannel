import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class SocketChannelServerBlocking {

	public static void main(String[] args) {
		
		// Declare variables to save the port (And it's defaults too!)
		int port = 8080;
		
		if(args.length == 1) { // If no argument given, just use the default one
			try {
				port = Integer.parseInt(args[0]);
				if(port < 1 || port > 65535) // If the port number is invalid, raise exception to use the default one
					throw new NumberFormatException("Invalid Port Number");
			} catch (NumberFormatException e) {
				port = 8080;
				System.err.println("Invalid port has been given. Will be using default port.");
			}
		}
		
		// Declare Important Variables
		ServerSocketChannel serverSocketChannel = null; // SocketChannel that serves the clients. Because it is a ServerSocketChannel!
		SocketChannel socketChannel = null; // For saving the Connected Clients 

		try {
			// Open the sockets and begin serving clients
			serverSocketChannel = ServerSocketChannel.open();
			serverSocketChannel.configureBlocking(true); // Set as Blocking Mode
			serverSocketChannel.socket().bind(new InetSocketAddress(port)); // Reserve the port
			System.out.println("[  i  ] Server has started with address " + serverSocketChannel.getLocalAddress());
			while((socketChannel = serverSocketChannel.accept()) != null){ // Always accept clients
				System.out.println("[  i  ] Got client, with address " + socketChannel.getLocalAddress());
				new Thread( new ServerThread(socketChannel) ).start(); // If new client comes, create new thread to serve that client
			}
			serverSocketChannel.close(); // Safely close the socket
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
